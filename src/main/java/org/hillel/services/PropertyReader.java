package org.hillel.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyReader {

  public Properties getProperties(String propertyFile) throws IOException {

    Properties properties = new Properties();
    try(InputStream is = this.getClass().getClassLoader().getResourceAsStream(propertyFile)) {
      properties.load(is);
    }

    return properties;
  }

}
